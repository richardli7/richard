﻿Feature: Shopping
As discussed, please complete an automation scenario using Selenium, C# and Specflow using the website automationpractice.com
	You need to complete one entire transaction using the best possible strategies and it needs to include proper verifications  as well.

Background: 
	Given I go to automationpractice.com

@mytag
Scenario: PlaceOrder
	Given I hover my mouse on women and click on summer dress
	And I hover my mouse on the first item and click add to cart, then click Proceed to checkout
	And I click proceed on the Summary page
	And I create random email on Signin page
	And I fill in addresses on Address page
	And I fill in shipping information on Shipping page
	And I fill in payment information on Payment page
	When I click the final button
	Then The information should say my order is successfully placed
