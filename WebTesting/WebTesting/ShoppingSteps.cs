﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TechTalk.SpecFlow;
using WebTesting.PageObjects;
using WebTesting.Singonton;

namespace WebTesting
{
    [Binding]
    public class ShoppingSteps
    {
        [Given(@"I go to automationpractice\.com")]
        public void GivenIGoToAutomationpractice_Com()
        {
            SeleniumWeb.Driver.Url = "http://www.automationpractice.com";
            //SeleniumWeb.Driver.Url = "http://automationpractice.com/index.php?id_category=11&controller=category";
        }
        
        [Given(@"I hover my mouse on women and click on summer dress")]
        public void GivenIHoverMyMouseOnWomenAndClickOnSummerDress()
        {
            HomePage homepage = new HomePage();
            homepage.HoverOnSummerWoman();
            homepage.ClickSummerDresses();
        }
        
        [Given(@"I hover my mouse on the first item and click add to cart, then click Proceed to checkout")]
        public void GivenIHoverMyMouseOnTheFirstItemAndClickAddToCartThenClickProceedToCheckout()
        {
            ItemSelectionPage itemselection = new ItemSelectionPage();
            itemselection.HoverOnFirstItem();
            itemselection.AddToCart();
            itemselection.ClickProceed();
        }
        
        [Given(@"I click proceed on the Summary page")]
        public void GivenIClickProceedOnThe_SummaryPage()
        {
            SummaryPage summary = new SummaryPage();
            summary.ClickProceed();
        }
        
        [Given(@"I create random email on Signin page")]
        public void GivenICreateRandomEmailOn_SignInPage()
        {
            SignInPage signIn = new SignInPage();
            signIn.Login();
        }
        
        [Given(@"I fill in addresses on Address page")]
        public void GivenIFillInAddressesOn_AddressPage()
        {
            AddressPage address = new AddressPage();
            address.ClickProceed();
        }
        
        [Given(@"I fill in shipping information on Shipping page")]
        public void GivenIFillInShippingInformationOn_ShippingPage()
        {
            ShippingPage shipping = new ShippingPage();
            shipping.ClickProceed();
        }
        
        [Given(@"I fill in payment information on Payment page")]
        public void GivenIFillInPaymentInformationOn_PaymentPage()
        {
            PaymentPage payment = new PaymentPage();
            payment.ClickProceed();
        }
        
        [When(@"I click the final button")]
        public void WhenIClickTheFinalButton()
        {
            ConfirmOrderPage confirm = new ConfirmOrderPage();
            confirm.ClickConfirmOrder();
        }
        
        [Then(@"The information should say my order is successfully placed")]
        public void ThenTheInformationShouldSayMyOrderIsSuccessfullyPlaced()
        {
            ConfirmOrderPage confirm = new ConfirmOrderPage();
            string message = confirm.GetMessage();
            Assert.AreEqual("Your order on My Store is complete.", message);
        }

        [AfterScenario()]
        public void AfterScenario()
        {
            MyScreen.Shot("Step _12.FinalPicture");
            SeleniumWeb.Driver.Quit();
        }
    }
}
