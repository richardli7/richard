﻿using OpenQA.Selenium;
using WebTesting.Singonton;

namespace WebTesting.PageObjects
{
    class SummaryPage
    {
        internal void ClickProceed()
        {
            IWebElement element = SeleniumWeb.Driver.FindElement(By.XPath("//*[@id='center_column']/p[2]/a[1]"));
            element.Click();
            MyScreen.Shot("Step 5.ClickProceed_FromSummaryPage");
        }
    }
}
