﻿using OpenQA.Selenium;
using WebTesting.Singonton;

namespace WebTesting.PageObjects
{
    class HomePage
    {
        public void HoverOnSummerWoman()
        {
            SeleniumWeb.Driver.FindElement(By.CssSelector("#block_top_menu > ul > li:nth-child(1)"));//implicit wait for this element to be ready
            SeleniumWeb.Driver.FindElement(By.CssSelector("#block_top_menu > ul > li:nth-child(1) > ul"));//implicit wait for this element to be ready
            SeleniumWeb.Driver.ExecuteScript(@"$('#block_top_menu > ul > li:nth-child(1) > ul').css('display','block')");
            MyScreen.Shot("Step 0.HoverOnSummerWoman");
        }

        internal void ClickSummerDresses()
        {
            IWebElement element = SeleniumWeb.Driver.FindElement(By.CssSelector("#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(2) > ul > li:nth-child(3) > a"));
            element.Click();
            MyScreen.Shot("Step 1.ClickSummerDresses");
        }
    }
}
