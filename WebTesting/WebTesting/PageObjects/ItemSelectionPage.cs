﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using WebTesting.Singonton;

namespace WebTesting.PageObjects
{
    class ItemSelectionPage
    {
        internal void HoverOnFirstItem()
        {
            SeleniumWeb.Driver.FindElement(By.XPath("//*[@id='center_column']/ul/li[1]"));//implicit wait for this element to be ready
            SeleniumWeb.Driver.ExecuteScript(@"$('#center_column > ul > li.ajax_block_product.col-xs-12.col-sm-6.col-md-4.first-in-line.last-line.first-item-of-tablet-line.first-item-of-mobile-line.last-mobile-line').addClass('hovered')");
            MyScreen.Shot("Step 2.HoverOnFirstItem");
        }

        internal void AddToCart()
        {
            IWebElement element = SeleniumWeb.Driver.FindElement(By.XPath("//*[@id='center_column']/ul/li[1]/div/div[2]/div[2]/a[1]"));
            element.Click();
            MyScreen.Shot("Step 3.AddToCart");
        }

        internal void ClickProceed()
        {
            WebDriverWait wait = new WebDriverWait(SeleniumWeb.Driver, TimeSpan.FromSeconds(SeleniumWeb.timeout));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='layer_cart']/div[1]/div[2]/div[4]/a")));
            MyScreen.Shot("Step 4.ClickProceed_From Item Select Page");
            element.Click();
        }
    }
}
