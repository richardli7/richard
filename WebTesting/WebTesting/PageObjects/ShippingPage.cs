﻿using OpenQA.Selenium;
using WebTesting.Singonton;

namespace WebTesting.PageObjects
{
    class ShippingPage
    {
        internal void ClickProceed()
        {
            IWebElement agree = SeleniumWeb.Driver.FindElement(By.Id("cgv"));
            IWebElement proceedButton = SeleniumWeb.Driver.FindElement(By.XPath("//*[@id='form']/p/button"));
            agree.Click();
            MyScreen.Shot("Step 8.ClickProceed_FromShippingPage");
            proceedButton.Click();
        }
    }
}
