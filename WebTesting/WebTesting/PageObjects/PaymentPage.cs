﻿using OpenQA.Selenium;
using WebTesting.Singonton;

namespace WebTesting.PageObjects
{
    class PaymentPage
    {
        internal void ClickProceed()
        {
            IWebElement payByCheck = SeleniumWeb.Driver.FindElement(By.CssSelector("#HOOK_PAYMENT > div:nth-child(2) > div > p > a"));
            payByCheck.Click();
            MyScreen.Shot("Step 9.ClickProceed_FromPaymentPage");
        }
    }
}
