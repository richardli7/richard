﻿using OpenQA.Selenium;
using WebTesting.Singonton;

namespace WebTesting.PageObjects
{
    class ConfirmOrderPage
    {
        internal void ClickConfirmOrder()
        {
            IWebElement payByCheck = SeleniumWeb.Driver.FindElement(By.CssSelector("#cart_navigation > button"));
            payByCheck.Click();
            MyScreen.Shot("Step _10.ClickConfirmOrder");
        }

        internal string GetMessage()
        {
            IWebElement promptMessage = SeleniumWeb.Driver.FindElement(By.CssSelector("#center_column > p.alert.alert-success"));
            MyScreen.Shot("Step _11.GetMessage");
            return promptMessage.Text;
        }
    }
}
