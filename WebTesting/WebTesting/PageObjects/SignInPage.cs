﻿using OpenQA.Selenium;
using WebTesting.Singonton;

namespace WebTesting.PageObjects
{
    class SignInPage
    {
        internal void Login()
        {
            string email = "richard.li.123@hotmail.com";
            string password = "123132";
            IWebElement element_email = SeleniumWeb.Driver.FindElement(By.Id("email"));
            IWebElement element_password = SeleniumWeb.Driver.FindElement(By.Id("passwd"));
            IWebElement login = SeleniumWeb.Driver.FindElement(By.Id("SubmitLogin"));
            element_email.SendKeys(email);
            element_password.SendKeys(password);
            MyScreen.Shot("Step 6.Fill Email and password and login");
            login.Click();
        }
    }
}
