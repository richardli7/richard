﻿using OpenQA.Selenium;
using WebTesting.Singonton;

namespace WebTesting.PageObjects
{
    class AddressPage
    {
        internal void ClickProceed()
        {
            IWebElement element = SeleniumWeb.Driver.FindElement(By.XPath("//*[@id='center_column']/form/p/button"));
            MyScreen.Shot("Step 7.ClickProceed_FromAddressPage");
            element.Click();
        }
    }
}
