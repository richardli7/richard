﻿using OpenQA.Selenium.Chrome;
using System;
using System.Configuration;

namespace WebTesting.Singonton
{
    class SeleniumWeb
    {
        private static ChromeDriver driver = null;
        public static int timeout = int.TryParse(ConfigurationManager.AppSettings["waittimeout"],out timeout)? timeout:15;

        public static ChromeDriver Driver
        {
            get
            {
                if (driver != null) return driver;
                else
                {
                    ChromeOptions chromeOptions = new ChromeOptions();
                    driver = new ChromeDriver(chromeOptions);
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    driver.Manage().Window.Maximize();
                    return driver;
                }
            }
        }
    }
}
