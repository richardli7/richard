﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace WebTesting.Singonton
{
    class MyScreen
    {
        public static void Shot(string name)
        {
            using (Bitmap bmpScreenCapture = new Bitmap(Screen.PrimaryScreen.Bounds.Width,Screen.PrimaryScreen.Bounds.Height))
            using (Graphics g = Graphics.FromImage(bmpScreenCapture))
            {
                g.CopyFromScreen(Screen.PrimaryScreen.Bounds.X,
                                 Screen.PrimaryScreen.Bounds.Y,
                                 0, 0,
                                 bmpScreenCapture.Size);
                if (!Directory.Exists("Screenshot")) Directory.CreateDirectory("Screenshot");
                string fileName = Path.Combine(Environment.CurrentDirectory, "Screenshot",$"{name}_{DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss")}.png");
                bmpScreenCapture.Save(fileName);
            }
        }
    }
}
